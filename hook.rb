require 'java'
# require './JNativeHook.jar'
require './JNativeHook2.jar'
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseInputListener;

class KeyHook
  include NativeKeyListener

  def nativeKeyPressed(e)
    puts NativeKeyEvent.getKeyText e.getKeyCode
  end

  def nativeKeyReleased(e)
    puts NativeKeyEvent.getKeyText e.getKeyCode
  end

  def nativeKeyTyped(e)
    puts NativeKeyEvent.getKeyText e.getKeyCode
  end

end

class MouseHook
  include NativeMouseInputListener

  def nativeMouseMoved(e)
    puts "#{e.getX}, #{e.getY}"
  end

end

GlobalScreen.registerNativeHook
p GlobalScreen.instance_methods.sort

GlobalScreen.getInstance.addNativeKeyListener KeyHook.new
GlobalScreen.getInstance.addNativeMouseMotionListener MouseHook.new
